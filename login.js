export default {
  init() {
    // JavaScript to be fired on the login page

    $('html').bind('keypress', function(e)
    {
      if(e.keyCode == 13)
      {
          return false;
      }
    });

    $('#loginform').addClass('uk-animation-fade').css('animation-delay','300ms');

    $('.login-submit').append('<a href="#" class="btn--login __green __lock">Entrar</a>');

    $('#wp-privacy').on('click', function(){
      if($(this).prop( "checked" )){
        $('#wp-submit').removeAttr('disabled');
        $('.login-submit .btn--login').removeClass('__lock');
        $('.login-privacy').addClass('__checked');
      }else{
        $('#wp-submit').attr('disabled');
        $('.login-submit .btn--login').addClass('__lock')
        $('.login-privacy').removeClass('__checked');
      }
    });

    $('.login-submit .btn--login').on('click', function(e){
      e.preventDefault();
      $('#wp-submit').trigger('click');
    });
    if( $('.error').length > 0 ){
      $('.login-password').after($('.error'));
      $('.error').addClass('visible'); 
    }

    $("#user_login").on("focusin", function(){
      $(this).attr('value','');
      $(this).siblings('label').addClass('show');
    });
    $("#user_login").on("focusout", function(){
      $(this).attr('value','Nome de utilizador');
      $(this).siblings('label').removeClass('show');
    });

    $("#user_pass").attr('value','Senha');
    $("#user_pass").on("focusin", function(){
      $(this).attr('value','');
      $(this).siblings('label').addClass('show');
    });
    $("#user_pass").on("focusout", function(){
      $(this).attr('value','Senha');
      $(this).siblings('label').removeClass('show');
    });
    
    
  },
  finalize() {
    // JavaScript to be fired on the login page, after the init JS
  },
};
