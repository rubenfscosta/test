{{--
  Template Name: Front page
--}}

@extends('layouts.app')

@section('content')
  <div id="fullpage">
    <div id="entrar-landing" class="section --entrar bg__pastel">
      <div class="uk-container">
        <div id="wrapper-paragraph" class="js-paragraph">
          <p id="paragraph" class="uk-margin-bottom h-landing">{!! App::title() !!}</p>
        </div>
        <a href="{!! App::pageLoginLink() !!}" class="btn--login __green">Entrar</a>
      </div>
      @include('partials.social')
      <a href="#" class="btn--scroll __trans __down"></a>
    </div>
    <div id="vantagens-landing" class="section --vantagens bg__white">
      <div class="uk-container">
        <div class="js-text --1 sub-heading-3 uk-margin-bottom">{{ the_field('title_vantagens') }}</div>
        <div class="js-text --2 big-text-input green uk-margin-bottom">{{ the_field('texto_1_vantagens') }}</div>
        <div class="js-text --3 small-text">{{ the_field('texto_2_vantagens') }}</div>
      </div>
    </div>
    <div id="faqs-landing" class="section --faqs bg__green">
      <div class="uk-container">
        <p class="title-faqs white heading-1">{{ the_field('title_faqs') }}</p>
      </div>
      <div uk-slider>
        <ul class="uk-slider-items">
            @foreach ($faqs as $faq)
              <li class="uk-width-1-1 uk-width-1-3@s js-faq">
                <p class="heading-2 white">{{ $faq->post_title }}</p>
                <p class="small-text-input2 white">{!! substr($faq->post_content, 0, 140) . '...' !!}</p>
              </li>
            @endforeach
        </ul>
        <ul class="uk-slider-nav uk-dotnav uk-flex-center"></ul>
        <a href="{!! App::pageLoginLink() !!}" class="btn--login __orange">Entrar</a>

      </div>
    </div>
    <div id="footer" class="section --footer fp-auto-height bg__green">
      @php do_action('get_footer') @endphp
      @include('partials.footer')
      @php wp_footer() @endphp
    </div>
  </div>
@endsection
